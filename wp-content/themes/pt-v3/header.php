<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="site-frame">

        <div class="dashboard-frame d-flex">

        <!-- =====[DASHBOARD ASIDE]===== -->
        <div class="dashboard-aside border-end position-fixed h-100">
               <div class="brand d-flex align-items-center justify-content-center border-bottom">
                  <img src="<?php echo T_ROOT; ?>/assets/img/brand.svg" alt="brand" class="w-100 d-block">
               </div>
               <nav class="menubar d-flex flex-column pt-3 pb-4">
                    <?php wp_nav_menu( array(
                        'menu'      => 'Primary Menu', 
                        'theme_location' => 'primary_menu',
                        'container' => false,
                        'menu_class' => 'menu dashboard-menu list-unstyled p-0',
                        ) );?>
                  
                  <?php wp_nav_menu( array(
                        'menu'      => 'Footer Menu', 
                        'theme_location' => 'footer_menu',
                        'container' => false,
                        'menu_class' => 'menu dashboard-menu list-unstyled p-0 mt-auto',
                    ) );?>
               </nav>
            </div>

            <!-- =====[DASHBOARD MAIN]===== -->
            <div class="dashboard-main d-flex flex-column">

               <!-- =====[DASHBOARD HEADER]===== -->
               <header class="dashboard-header d-flex align-items-center justify-content-between">
                  <?php if(wp_is_mobile()) {?>
                     <button class="btn btn-header-search d-inline-flex align-items-center px-0 rounded-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                        Search
                     </button>
                  <?php } else{?>
                     <div class="dashboard-header-title"><?php the_title(); ?></div>
                  <?php } ?>
                  <div class="dashboard-header-options d-flex align-items-center">
                     <button type="button" class="btn btn-header-item-add d-inline-flex align-items-center justify-content-center rounded-3" data-bs-toggle="modal" data-bs-target="#modalNewOnboarding">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                        Add New Project
                     </button>
                     <button class="btn btn-aside-open">
                        <span></span>
                        <span></span>
                        <span></span>
                     </button>
                  </div>
               </header>