jQuery(document).ready(function($){

	//STICKY HEADER
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();
	    if (scroll >= 1) {
	        $(".site-header").addClass("scrolled");
	    } else {
	        $(".site-header").removeClass("scrolled");
	    }
	});


	// DASHBOARD ASIDE SHOW OR HIDE
	$('.btn-aside-open').on('click',function(){
		$('.dashboard-aside').toggleClass('open');
		$(this).toggleClass('open');
	});


	// DATA MIN CARD CAROUSEL
	$('.data-min-card-carousel').owlCarousel({
		loop:false,
		margin: 20,
		nav:true,
		dots: true,
		items: 6,
		autoWidth:true,
	});


	// DATA TABLE TRAY
	$('.btn-data-table-close').on('click',function(){
		$('.data-table-top-tray').removeClass('is-visible');
	});


	//JQUERY DATAPICKER
	$('.form-control-date').datepicker({
        format: "dd-M-yyyy",
        clearBtn: true,
        autoclose: true,
		todayHighlight: true
    });


	//FLOATING BUTTON
	$('.btn-float-main').on('click',function(){
		$('.floating-button-group').toggleClass('open');
	});


	//WYSIWYG EDITER
	$.trumbowyg.svgPath = frontend_ajax_object.templateUrl + '/assets/img/wysiwyg-icons.svg';
	$('#emailMessageTextarea').trumbowyg({
		btns: [
			['formatting'],
			['strong', 'em', 'del'],
			['superscript', 'subscript'],
			['link'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['unorderedList', 'orderedList'],
			['horizontalRule'],
			['removeformat'],
			['preformatted'],
			['undo', 'redo'],
			['viewHTML'],
		],
		changeActiveDropdownIcon: true,
		autogrow: false,
	});


});