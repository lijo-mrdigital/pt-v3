jQuery(document).ready(function($){

    $(document).on('click', '.briefing, .pt-setup, .prod_bud_setup, .payment_setup, .access_received, .gds_setup, .meeting_pref', function(e){
        e.preventDefault();
        var elem = $(this),
        post_id = elem.data('id'),
        status = elem.data('status'),
        meta = elem.data('meta');
        $.ajax({
            type: "POST",
            url: frontend_ajax_object.ajaxurl,
            data: { action: 'update_client', post_id: post_id, status: status, meta: meta }
        }).done(function( response ) {
            if( response ){
                var msg = JSON.parse(response);
                if(msg.status == 'Overdue')
                    var html = '<span class="dropdown-data-text-active dropdown-data-text-active-danger">'+ msg.status +'<span class="close ms-2"></span> </span>';
                else
                    var html = '<span class="dropdown-data-text-active">'+ msg.status +'<span class="close ms-2"></span> </span>';
                var prevHtml = '<span class="dropdown-data-text-muted">Add Status</span>';
                elem.parents('.dropdown-data-status').addClass('active').find('.btn.dropdown-toggle').html(prevHtml + html);
            }
        });
    });

    $(document).on('change', '.date-deadline', function(){
        var post_id = $(this).data('id'),
        status = $(this).val(),
        meta = 'deadline';
        $.ajax({
            type: "POST",
            url: frontend_ajax_object.ajaxurl,
            data: { action: 'update_client', post_id: post_id, status: status, meta: meta }
        }).done(function( response ) {
            if( response ){
                var msg = JSON.parse(response);
                console.log(msg);
            }
        });
    });

    $(document).on('click', '.dropdown-data-status .close', function(){
        var elemButton = $(this).parents('.dropdown-toggle');
        var post_id = elemButton.data('id'),
        meta = elemButton.data('meta');
        $.ajax({
            type: "POST",
            url: frontend_ajax_object.ajaxurl,
            data: { action: 'delete_client_meta', post_id: post_id, meta: meta }
        }).done(function( response ) {
            if( response ){
                var msg = JSON.parse(response);
                if(msg.updated == true){
                    $(elemButton).html('<span class="dropdown-data-text-muted">Add Status</span>').parent().removeClass('active');
                }
            }
        });
    });

    $(document).on('click', '.show-download-strip', function(){
        var cnt = 0;
        $('.client-tr').each(function(){
            var checkBox = $(this).find('input.show-download-strip');
            if($(checkBox).is(':checked')) 
                cnt++; 
        });
        $('#client-count').html(cnt);
        if(cnt > 0)
            $('.download-strip').addClass('is-visible');
        else
            $('.download-strip').removeClass('is-visible');
    });

    $(document).on('click', '.delete-client', function(){
        var client = [];
        $('.client-tr').each(function(){
            var checkBox = $(this).find('input.show-download-strip');
            if($(checkBox).is(':checked')) 
                client.push($(checkBox).val());
        });
        if(client.length > 0){
            $.ajax({
                type: "POST",
                url: frontend_ajax_object.ajaxurl,
                data: { action: 'delete_client', post_id: client }
            }).done(function( response ) {
                if( response ){
                    var clients = JSON.parse(response);
                    $.each(clients, function(index, client){
                        $('.client-tr').each(function(){
                            if($(this).data('id') == client) 
                                $(this).addClass('tr-data-deleted');
                        });
                    });
                    $('#hide-deleted').show();
                }
            });
        }
    });
    
    $(document).on('click', '#hide-deleted', function(){
        if ($(this).find('.text').text() == "Hide Deleted") $(this).find('.text').text("Show Deleted");
        else $(this).find('.text').text("Hide Deleted");
        $('.client-tr.tr-data-deleted').slideToggle();
    });

    $('#add-project').validate({
        rules: {
            project_logo: {
                required: true,
                filesize: 30000,
            }
        },
        submitHandler: function(e) { 
            
        }
    });

    $("form#add-project").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);
        //var fileData = $('#formFile').prop('files')[0];
        //formData.append('file', fileData);
        formData.append('action', 'add_project');
        formData.append('security', frontend_ajax_object.security);
        
        //data: { action: 'add_project', formData: 'tes' },
    
        $.ajax({
            type: "POST",
            url: frontend_ajax_object.ajaxurl,
            processData: false,
            contentType: false,
            data: formData
        }).done(function( response ) {
            if( response ){
                
            }
        });
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    },'File size must be less than {0}kb');

    $(document).on('click', '#download-client', function(){
        var clients = [];
        $('.client-tr').each(function(){
            var checkBox = $(this).find('input.show-download-strip');
            if( $(checkBox).prop('checked') == true ) clients.push($(checkBox).val());
        });
        if(clients.length > 0){
            $.ajax({
                type: "POST",
                url: frontend_ajax_object.ajaxurl,
                data: { action: 'download_client', clients: clients }
            }).done(function( response ) {
                console.log(response);
            });
        }
    });

    $(document).on('click', '#send-email', function(e){
        e.preventDefault();
        $(this).parents('.modal').find('.wpforms-submit').trigger('click');
    });

});

function searchClient() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search-client");
    filter = input.value.toUpperCase();
    ul = document.getElementById("client-list");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("details")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function sortTable(table, col, reverse) {
    var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
        tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
        i;
    reverse = -((+reverse) || -1);
    tr = tr.sort(function (a, b) { // sort rows
        return reverse // `-1 *` if want opposite order
            * (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
                .localeCompare(b.cells[col].textContent.trim())
               );
    });
    for(i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
}

function makeSortable(table) {
    var th = table.tHead, i;
    th && (th = th.rows[0]) && (th = th.cells);
    if (th) i = th.length;
    else return; // if no `<thead>` then do nothing
    while (--i >= 0) (function (i) {
        var dir = 1;
        th[i].addEventListener('click', function () {sortTable(table, i, (dir = 1 - dir))});
    }(i));
}

function makeAllSortable(parent) {
    parent = parent || document.body;
    var t = parent.getElementsByTagName('table'), i = t.length;
    while (--i >= 0) makeSortable(t[i]);
}

window.onload = function () {makeAllSortable();};