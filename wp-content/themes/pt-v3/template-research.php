<?php
/* Template name: Research */
get_header(); ?>
<?php if ( wp_is_mobile() ) : ?>
   <!-- =====[DASHBOARD CONTAINER]===== -->
   <div class="dashboard-container h-100">
      <div class="dashboard-main-title-panel container-fluid py-3"><h1 class="fz-16 fw-semi-bold mb-0">Onboarding</h1></div>
         <div class="dashboard-filter-panel container-fluid d-flex align-items-center justify-content-end py-1">
            <div class="col-auto me-3">
               <button class="btn btn-item-show rounded-3 px-2 py-1" id="hide-deleted">Hide Deleted (1)</button>
            </div>
            <div class="col-auto data-filter-dropdowns">
               <div class="dropdown data-filter-dropdown">
                  <button class="btn dropdown-toggle" type="button" id="dataFilter1" data-bs-toggle="dropdown" aria-expanded="false">Filter By</button>
                  <ul class="dropdown-menu" aria-labelledby="dataFilter1" style="">
                     <li>
                        <a class="dropdown-item" href="#">Client</a>
                        <button class="btn dropdown-item-delete">
                           <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </button>
                     </li>
                     <li>
                        <a class="dropdown-item" href="#">Selected</a>
                        <button class="btn dropdown-item-delete">
                           <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </button>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="dashboard-main-content container-fluid">
            <ul class="list-unstyled task-list task-list-onboarding">
            <?php echo get_template_part('parts/client', 'table-research-mob');?>  
            </ul>
         </div>
      </div>
<?php else : ?>
<!-- =====[DASHBOARD CONTAINER]===== -->
<div class="dashboard-container h-100">
                  <div class="main-content-panel  d-flex overflow-hidden">

                     <!-- =====[ASIDE]===== -->
                     <div class="mcp-aside">
                        <div class="mcp-aside-search position-relative border-end border-bottom bg-white">
                           <input type="text" class="form-control mcp-aside-search-input border-0 rounded-0" placeholder="Search Clients" onkeyup="searchClient()" id="search-client" />
                           <span class="mcp-aside-search-icon position-absolute top-50 lh-1">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                           </span>
                        </div>
                        <?php echo get_template_part('parts/client', 'list');?>
                     </div>

                     <!-- =====[MAIN]===== -->
                     <div class="mcp-main position-relative">
                        <div class="data-table-top-tray download-strip position-fixed">
                           <div class="data-table-top-tray-inner d-flex align-items-center">
                              <div class="data-table-tray-label"><span id="client-count"></span> Items Selected</div>
                              <ul class="data-table-tray-menu d-flex align-items-center list-unstyled p-0 mb-0">
                                 <li class="menu-item">
                                    <button class="menu-link" id="download-client">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg>
                                       Download
                                    </button>
                                 </li>
                                 <li class="menu-item">
                                    <button class="menu-link delete-client">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                       Delete
                                    </button>
                                 </li>
                                 <li class="menu-item">
                                    <button class="menu-link">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                       Bulk Mail
                                    </button>
                                 </li>
                              </ul>
                              <button class="btn btn-data-table-close lh-1 ms-auto">
                                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                              </button>
                           </div>
                        </div>
                        <div class="data-filter-wrapper row align-items-center pt-0 pb-2">
                           <h5 class="fz-14 fw-semi-bold col-auto mb-0 me-auto">Recent Clients</h5>
                           <div class="col-auto">
                              <button id="hide-deleted" class="btn btn-item-show rounded-3 px-2 py-1"><span class="text">Hide Deleted</span> (2)</button>
                           </div>
                           <div class="data-filter-dropdowns col-auto">
                              <div class="dropdown data-filter-dropdown">
                                 <button class="btn dropdown-toggle" type="button" id="dataFilter1" data-bs-toggle="dropdown" aria-expanded="false">Filter By</button>
                                 <ul class="dropdown-menu" aria-labelledby="dataFilter1">
                                    <li>
                                       <a class="dropdown-item" href="#">Client</a>
                                       <button class="btn dropdown-item-delete">
                                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                       </button>
                                    </li>
                                    <li>
                                       <a class="dropdown-item" href="#">Selected</a>
                                       <button class="btn dropdown-item-delete">
                                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                       </button>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="data-min-cards">
                           <?php echo get_template_part('parts/recent', 'clients');?>                                                  
                        </div>                        
                        <?php echo get_template_part('parts/client', 'table-research');?>                        
                        
                     </div>
                  </div>
               </div>
<?php endif; ?>
<?php get_footer(); ?>