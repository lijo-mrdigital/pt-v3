<?php
$ts = new Theme_Customisations();
global $wpdb;
$table_name = $wpdb->prefix . 'recent_clients';
$user_id = get_current_user_id();
$clients = $wpdb->get_results( "SELECT DISTINCT client_id FROM $table_name WHERE user_id = $user_id ORDER BY id DESC" );
if(!empty($clients)){
?>
<div class="owl-carousel data-min-card-carousel">
    <?php foreach($clients as $client) {?>
    <div class="item">
        <div class="data-min-card d-flex align-items-center border rounded position-relative">
            <?php if(has_post_thumbnail($client->client_id)){
                ?><div class="data-min-card-icon-box rounded p-1 d-inline-flex align-items-center justify-content-center"><?php
                echo get_the_post_thumbnail($client->client_id, '');
                ?></div><?php
            }
            else {
                ?><div class="data-min-card-icon-box dmc-letters rounded p-1 d-inline-flex align-items-center justify-content-center"><?php
                echo $ts->get_client_title($client->client_id);
                ?></div><?php
            } ?>
        <div class="data-min-card-content flex-grow-1 overflow-hidden">
            <div class="data-min-card-title fw-medium text-truncate text-nowrap overflow-hidden"><?php echo get_the_title($client->client_id); ?></div>
            <div class="data-min-card-title-min text-truncate text-nowrap overflow-hidden"><?php 
            $updated = get_the_modified_date('d-m-Y H:i:s', $client->client_id);
            $now = date('d-m-Y H:i:s'); 
            
            $date1 = date_create($now);
            $date2 = date_create($updated);
            $diff = date_diff($date1,$date2);

            if( $diff->y > 0 ) echo __('More than a year ago');
            elseif( $diff->m > 0 ) echo __('More than a month ago');
            elseif($diff->d > 0 ) echo $diff->d . __(' day(s) ago');
            elseif($diff->h > 0 ) echo $diff->h . __(' hour(s) ago');
            else echo $diff->i . __(' minute(s) ago');
            ?></div>
        </div>
        <a href="<?php echo get_permalink($client->client_id) ?>" class="data-min-card-link position-absolute top-0 start-0 w-100 h-100"></a>
        </div>
    </div>
    <?php } ?>
</div>
<?php } ?>