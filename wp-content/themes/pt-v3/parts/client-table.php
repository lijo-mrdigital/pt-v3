<?php 
$ts = new Theme_Customisations();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$per_page = 20;
$args = array('post_type' => 'client', 'posts_per_page' => $per_page, 'paged' => $paged);
$query = new WP_Query($args);
$total_pages = ceil( $query->found_posts / $per_page );
if($query->have_posts()){
    ?><div class="d-flex align-items-center justify-content-between pt-4 pb-3">
    <h5 class="fz-14 fw-semi-bold mb-0">Clients</h5>
    <div class="page-counters d-flex align-items-center">
       <div class="page-counter-no fw-medium">Page 1 of <?php echo $total_pages; ?></div>
       <div class="page-counter-nav d-inline-flex align-items-center">
            <?php if ( get_previous_posts_link() ) { ?>
            <a href="<?php echo get_pagenum_link( $paged - 1 ); ?>" class="page-counter-prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>
            </a><?php
            }    
            if ( get_next_posts_link(null,$per_page) ) { ?>
            <a href="<?php echo get_pagenum_link( $paged +1 ); ?>" class="page-counter-next">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
            </a><?php
            }?>
       </div>
    </div>
 </div>
 <div class="table-data-responsive table-data-responsive-onboarding">
    <table class="table table-data table-bordered">
        <thead>
            <tr>
            <th class="col-pt-clients">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd1" data-bs-toggle="dropdown" aria-expanded="false">
                        Client
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dd1">
                        <li><a class="dropdown-item" href="#">A - Z</a></li>
                        <li><a class="dropdown-item" href="#">Z - A</a></li>
                        <li><a class="dropdown-item" href="#">Date Added</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <span class="span-box d-inline-block">Communication</span>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd2" data-bs-toggle="dropdown" aria-expanded="false">
                        Client / Project Brief
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd2">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd3" data-bs-toggle="dropdown" aria-expanded="false">
                        Performance Tracker Setup
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd3">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd4" data-bs-toggle="dropdown" aria-expanded="false">
                        Productive & Budge Setup
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd4">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd5" data-bs-toggle="dropdown" aria-expanded="false">
                        Payment Setup
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd5">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd6" data-bs-toggle="dropdown" aria-expanded="false">
                        Access Received
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd6">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd7" data-bs-toggle="dropdown" aria-expanded="false">
                        Google Data Studio Setup
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd7">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd8" data-bs-toggle="dropdown" aria-expanded="false">
                        Activity Launch Deadline
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd8">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            <th class="col-pt-default">
                <div class="dropdown th-data-dropdown">
                    <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd9" data-bs-toggle="dropdown" aria-expanded="false">
                        Meeting Preference
                    </button>
                    <ul class="dropdown-menu shadow-sm" aria-labelledby="dd9">
                        <li><a class="dropdown-item" href="#">Done</a></li>
                        <li><a class="dropdown-item" href="#">Overdue</a></li>
                    </ul>
                </div>
            </th>
            </tr>
        </thead>
        <tbody><?php
            $delCount = 0;
            while($query->have_posts()){ $query->the_post();
                $deleted = get_post_meta($post->ID, 'deleted', true);
                $cssClass = ( $deleted ) ? ' tr-data-deleted' : '';
                if($deleted) $delCount++;
            ?><tr data-id="<?php the_id(); ?>" class="client-tr <?php echo $cssClass; ?>">
            <td class="col-pt-clients">
                <div class="td-boxs d-inline-flex align-items-center w-100">
                    <div class="td-checkbox-box">
                        <input name="show-download-strip[]" class="form-check-input td-checkbox show-download-strip" type="checkbox" id="chk<?php the_id(); ?>" value="<?php the_id(); ?>">
                    </div>
                    <div class="td-client-box d-inline-flex align-items-center">
                        <?php if(has_post_thumbnail()){
                            ?><div class="td-icon-box tib-media d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'd-block mx-auto w-100' ));
                            ?></div><?php
                        }
                        else {
                            ?><div class="td-icon-box tib-letter d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo $ts->get_client_title($post->ID);
                            ?></div><?php
                        } ?>
                        <div class="td-title fw-light"><strong class="fw-medium"><?php the_title(); ?></strong></div>
                    </div>
                </div>
            </td>
            <td class="col-pt-default">
                <!-- <button class="btn btn-send-mail fz-12 d-inline-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                    Send Mail
                </button> -->
                <button type="button" class="btn btn-send-mail fz-12 d-inline-flex align-items-center ms-2" data-bs-toggle="modal" data-bs-target=".modal-mail-start">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                            Send Mail
                        </button>
            </td>
            <td class="col-pt-default">
                <?php $briefing = get_post_meta( $post->ID, 'briefing', true ); ?>
                <div class="dropdown dropdown-data-status briefing-status <?php if($briefing) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds1" data-bs-toggle="dropdown" aria-expanded="false" data-meta="briefing" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($briefing) {
                            ?><span class="dropdown-data-text-active <?php if($briefing == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $briefing; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <?php if(! $deleted) {?>
                    <ul class="dropdown-menu" aria-labelledby="dds1">
                        <li><a class="dropdown-item dropdown-item-data-success briefing" href="#" data-meta="briefing" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger briefing" href="#" data-meta="briefing" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                    <?php }?>
                </div>
            </td>
            <td class="col-pt-default">
                <?php $pt_setup = get_post_meta( $post->ID, 'pt_setup', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($pt_setup) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds2" data-bs-toggle="dropdown" aria-expanded="false" data-meta="pt_setup" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($pt_setup) {
                            ?><span class="dropdown-data-text-active <?php if($pt_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $pt_setup; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds2">
                        <li><a class="dropdown-item dropdown-item-data-success pt-setup" href="#" data-meta="pt_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger pt-setup" href="#" data-meta="pt_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                </div>
            </td>
            <td class="col-pt-default">
                <?php $prod_bud_setup = get_post_meta( $post->ID, 'prod_bud_setup', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($prod_bud_setup) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds3" data-bs-toggle="dropdown" aria-expanded="false" data-meta="prod_bud_setup" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($prod_bud_setup) {
                            ?><span class="dropdown-data-text-active <?php if($prod_bud_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $prod_bud_setup; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds3">
                        <li><a class="dropdown-item dropdown-item-data-success prod_bud_setup" href="#" data-meta="prod_bud_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger prod_bud_setup" href="#" data-meta="prod_bud_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                </div>
            </td>
            <td class="col-pt-default">
                <?php $payment_setup = get_post_meta( $post->ID, 'payment_setup', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($payment_setup) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds4" data-bs-toggle="dropdown" aria-expanded="false" data-meta="payment_setup" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($payment_setup) {
                            ?><span class="dropdown-data-text-active <?php if($payment_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $payment_setup; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds4">
                        <li><a class="dropdown-item dropdown-item-data-success payment_setup" href="#" data-meta="payment_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger payment_setup" href="#" data-meta="payment_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                </div>
            </td>
            <td class="col-pt-default">
                <?php $access_received = get_post_meta( $post->ID, 'access_received', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($access_received) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds5" data-bs-toggle="dropdown" aria-expanded="false" data-meta="access_received" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($access_received) {
                            ?><span class="dropdown-data-text-active <?php if($access_received == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $access_received; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds5">
                        <li><a class="dropdown-item dropdown-item-data-success access_received" href="#" data-meta="access_received" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger access_received" href="#" data-meta="access_received" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                </div>
            </td>
            <td class="col-pt-default">
                <?php $gds_setup = get_post_meta( $post->ID, 'gds_setup', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($gds_setup) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds6" data-bs-toggle="dropdown" aria-expanded="false" data-meta="gds_setup" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Status</span>
                        <?php if($gds_setup) {
                            ?><span class="dropdown-data-text-active <?php if($gds_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $gds_setup; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds6">
                        <li><a class="dropdown-item dropdown-item-data-success gds_setup" href="#" data-meta="gds_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                        <li><a class="dropdown-item dropdown-item-data-danger gds_setup" href="#" data-meta="gds_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                    </ul>
                </div>
            </td>
            <td class="col-pt-default">
                <div class="td-form-group">
                    <?php $deadline = get_post_meta( $post->ID, 'deadline', true ); ?>
                    <input type="text" class="form-control form-control-date rounded-0 border-0 date-deadline" data-id="<?php the_id(); ?>" value="<?php echo ($deadline) ? $deadline : ''; ?>" placeholder="dd-mm-Y">
                </div>
            </td>
            <td class="col-date">
                <?php $meeting_pref = get_post_meta( $post->ID, 'meeting_pref', true ); ?>
                <div class="dropdown dropdown-data-status <?php if($meeting_pref) echo 'active'; ?>">
                    <button class="btn dropdown-toggle" type="button" id="dds7" data-bs-toggle="dropdown" aria-expanded="false" data-meta="meeting_pref" data-id="<?php the_id(); ?>">
                        <span class="dropdown-data-text-muted">Add Preference</span>
                        <?php if($meeting_pref) {
                            ?><span class="dropdown-data-text-active <?php if($meeting_pref == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $meeting_pref; ?><span class="close ms-2"></span> </span><?php 
                        } ?>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dds7">
                        <li><a class="dropdown-item dropdown-item-data-default meeting_pref" href="#" data-meta="meeting_pref" data-status="30 min/ Fortnight" data-id="<?php the_id(); ?>">30 min/ Fortnight</a></li>
                        <li><a class="dropdown-item dropdown-item-data-default meeting_pref" href="#" data-meta="meeting_pref" data-status="60 min/ Month" data-id="<?php the_id(); ?>">60 min/ Month</a></li>
                    </ul>
                </div>
            </td>
        </tr><?php
        }
    ?></table>
</div>
<?php
    $ts->pt_v3_numeric_posts_nav($query); 
    ?><script><?php
        if($delCount > 0){?>
            document.getElementById("hide-deleted").innerHTML = "Hide Deleted (<?php echo $delCount; ?>)";
        <?php }
        else {?>
            document.getElementById("hide-deleted").style.display = "none";
        <?php }?>
        </script>
    <?php
}
else echo __('Nothing found!');
wp_reset_postdata();
?>