<?php 
$ts = new Theme_Customisations();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$per_page = 20;
$args = array('post_type' => 'client', 'posts_per_page' => $per_page, 'paged' => $paged);
$query = new WP_Query($args);
$total_pages = ceil( $query->found_posts / $per_page );
if($query->have_posts()){
    ?><?php
            while($query->have_posts()){ $query->the_post();
                $deleted = get_post_meta($post->ID, 'deleted', true);
                $cssClass = ( $deleted ) ? ' task-list-item-deleted' : '';
            ?><li class="task-list-item <?php echo $cssClass; ?>">
            <div class="task-panel position-relative">
               <div class="task-checkbox-box position-absolute start-0">
                  <input name="show-download-strip[]" class="form-check-input task-checkbox show-download-strip" type="checkbox" id="chk<?php the_id(); ?>" value="<?php the_id(); ?>">
               </div>
               <h2 class="task-title fz-14 fw-light mb-2"><strong class="d-inline fw-medium"><?php the_title(); ?></strong></h2>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Communication</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <button type="button" class="btn btn-send-mail fz-12 d-inline-flex align-items-center ms-2" data-bs-toggle="modal" data-bs-target=".modal-mail-start">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                        Send Mail
                     </button>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <?php $briefing = get_post_meta( $post->ID, 'briefing', true ); ?>
                  <div class="task-box-label">Client / Project Brief </div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <div class="dropdown dropdown-data-status briefing-status <?php if($briefing) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds1" data-bs-toggle="dropdown" aria-expanded="false" data-meta="briefing" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($briefing) {
                                 ?><span class="dropdown-data-text-active <?php if($briefing == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $briefing; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <?php if(! $deleted) {?>
                        <ul class="dropdown-menu" aria-labelledby="dds1">
                              <li><a class="dropdown-item dropdown-item-data-success briefing" href="#" data-meta="briefing" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger briefing" href="#" data-meta="briefing" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                        <?php }?>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Performance Tracker Setup</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $pt_setup = get_post_meta( $post->ID, 'pt_setup', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($pt_setup) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds2" data-bs-toggle="dropdown" aria-expanded="false" data-meta="pt_setup" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($pt_setup) {
                                 ?><span class="dropdown-data-text-active <?php if($pt_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $pt_setup; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds2">
                              <li><a class="dropdown-item dropdown-item-data-success pt-setup" href="#" data-meta="pt_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger pt-setup" href="#" data-meta="pt_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Productive & Budge Setup</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $prod_bud_setup = get_post_meta( $post->ID, 'prod_bud_setup', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($prod_bud_setup) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds3" data-bs-toggle="dropdown" aria-expanded="false" data-meta="prod_bud_setup" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($prod_bud_setup) {
                                 ?><span class="dropdown-data-text-active <?php if($prod_bud_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $prod_bud_setup; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds3">
                              <li><a class="dropdown-item dropdown-item-data-success prod_bud_setup" href="#" data-meta="prod_bud_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger prod_bud_setup" href="#" data-meta="prod_bud_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Payment Setup :</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $payment_setup = get_post_meta( $post->ID, 'payment_setup', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($payment_setup) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds4" data-bs-toggle="dropdown" aria-expanded="false" data-meta="payment_setup" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($payment_setup) {
                                 ?><span class="dropdown-data-text-active <?php if($payment_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $payment_setup; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds4">
                              <li><a class="dropdown-item dropdown-item-data-success payment_setup" href="#" data-meta="payment_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger payment_setup" href="#" data-meta="payment_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Access Received</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $access_received = get_post_meta( $post->ID, 'access_received', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($access_received) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds5" data-bs-toggle="dropdown" aria-expanded="false" data-meta="access_received" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($access_received) {
                                 ?><span class="dropdown-data-text-active <?php if($access_received == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $access_received; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds5">
                              <li><a class="dropdown-item dropdown-item-data-success access_received" href="#" data-meta="access_received" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger access_received" href="#" data-meta="access_received" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Google Data Studio Setup</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $gds_setup = get_post_meta( $post->ID, 'gds_setup', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($gds_setup) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds6" data-bs-toggle="dropdown" aria-expanded="false" data-meta="gds_setup" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Status</span>
                              <?php if($gds_setup) {
                                 ?><span class="dropdown-data-text-active <?php if($gds_setup == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $gds_setup; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds6">
                              <li><a class="dropdown-item dropdown-item-data-success gds_setup" href="#" data-meta="gds_setup" data-status="Done" data-id="<?php the_id(); ?>">Done</a></li>
                              <li><a class="dropdown-item dropdown-item-data-danger gds_setup" href="#" data-meta="gds_setup" data-status="Overdue" data-id="<?php the_id(); ?>">Overdue</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Activity Launch Deadline</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <div class="task-form-group">
                        <?php $deadline = get_post_meta( $post->ID, 'deadline', true ); ?>
                        <input type="text" class="form-control form-control-date rounded-0 border-0 d-inline-block date-deadline" data-id="<?php the_id(); ?>" value="<?php echo ($deadline) ? $deadline : ''; ?>" placeholder="dd-mm-Y">
                     </div>
                  </div>
               </div>
               <div class="task-box d-inline-flex align-items-center">
                  <div class="task-box-label">Meeting Preference</div>
                  <span class="task-box-sep ps-2">:</span>
                  <div class="task-box-main">
                     <?php $meeting_pref = get_post_meta( $post->ID, 'meeting_pref', true ); ?>
                     <div class="dropdown dropdown-data-status <?php if($meeting_pref) echo 'active'; ?>">
                        <button class="btn dropdown-toggle" type="button" id="dds7" data-bs-toggle="dropdown" aria-expanded="false" data-meta="meeting_pref" data-id="<?php the_id(); ?>">
                              <span class="dropdown-data-text-muted">Add Preference</span>
                              <?php if($meeting_pref) {
                                 ?><span class="dropdown-data-text-active <?php if($meeting_pref == 'Overdue') echo ' dropdown-data-text-active-danger'; ?>"><?php echo $meeting_pref; ?><span class="close ms-2"></span> </span><?php 
                              } ?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dds7">
                              <li><a class="dropdown-item dropdown-item-data-default meeting_pref" href="#" data-meta="meeting_pref" data-status="30 min/ Fortnight" data-id="<?php the_id(); ?>">30 min/ Fortnight</a></li>
                              <li><a class="dropdown-item dropdown-item-data-default meeting_pref" href="#" data-meta="meeting_pref" data-status="60 min/ Month" data-id="<?php the_id(); ?>">60 min/ Month</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </li><?php
        }
    ?>
<?php $themeSetup = new Theme_Customisations();
    $themeSetup->pt_v3_numeric_posts_nav($query); 
}
else echo __('Nothing found!');
wp_reset_postdata();
?>