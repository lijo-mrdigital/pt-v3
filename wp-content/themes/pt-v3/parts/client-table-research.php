<?php 
$ts = new Theme_Customisations();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$per_page = 20;
$args = array('post_type' => 'client', 'posts_per_page' => $per_page, 'paged' => $paged);
$query = new WP_Query($args);
$total_pages = ceil( $query->found_posts / $per_page );
if($query->have_posts()){
    ?><div class="d-flex align-items-center justify-content-between pt-4 pb-3">
    <h5 class="fz-14 fw-semi-bold mb-0">Clients</h5>
    <div class="page-counters d-flex align-items-center">
       <div class="page-counter-no fw-medium">Page 1 of <?php echo $total_pages; ?></div>
       <div class="page-counter-nav d-inline-flex align-items-center">
            <?php if ( get_previous_posts_link() ) { ?>
            <a href="<?php echo get_pagenum_link( $paged - 1 ); ?>" class="page-counter-prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>
            </a><?php
            }    
            if ( get_next_posts_link(null,$per_page) ) { ?>
            <a href="<?php echo get_pagenum_link( $paged +1 ); ?>" class="page-counter-next">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
            </a><?php
            }?>
       </div>
    </div>
 </div>
 <div class="table-data-responsive table-data-responsive-onboarding">
    <table class="table table-data table-bordered">
        <thead>
            <tr>
                <th class="col-pt-clients">
                    <div class="dropdown th-data-dropdown">
                        <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd1" data-bs-toggle="dropdown" aria-expanded="false">
                            Client
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dd1">
                            <li><a class="dropdown-item" href="#">A - Z</a></li>
                            <li><a class="dropdown-item" href="#">Z - A</a></li>
                            <li><a class="dropdown-item" href="#">Date Added</a></li>
                        </ul>
                    </div>
                </th>
                <th class="col-pt-default">
                    <div class="dropdown th-data-dropdown">
                        <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd6" data-bs-toggle="dropdown" aria-expanded="false">
                            Briefs
                        </button>
                        <ul class="dropdown-menu shadow-sm" aria-labelledby="dd6">
                            <li><a class="dropdown-item" href="#">Yes</a></li>
                            <li><a class="dropdown-item" href="#">No</a></li>
                        </ul>
                    </div>
                </th>
                <th class="col-pt-default">
                    <div class="dropdown th-data-dropdown">
                        <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd7" data-bs-toggle="dropdown" aria-expanded="false">
                            Messaging
                        </button>
                        <ul class="dropdown-menu shadow-sm" aria-labelledby="dd7">
                            <li><a class="dropdown-item" href="#">Top</a></li>
                            <li><a class="dropdown-item" href="#">Less</a></li>
                        </ul>
                    </div>
                </th>
                <th class="col-pt-default">
                    <div class="dropdown th-data-dropdown">
                        <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd8" data-bs-toggle="dropdown" aria-expanded="false">
                            Audience Profile
                        </button>
                        <ul class="dropdown-menu shadow-sm" aria-labelledby="dd8">
                            <li><a class="dropdown-item" href="#">Top</a></li>
                            <li><a class="dropdown-item" href="#">Less</a></li>
                        </ul>
                    </div>
                </th>
                <th class="col-pt-default">
                    <div class="dropdown th-data-dropdown">
                        <button class="btn dropdown-toggle rounded-0 w-100" type="button" id="dd9" data-bs-toggle="dropdown" aria-expanded="false">
                            Action
                        </button>
                        <ul class="dropdown-menu shadow-sm" aria-labelledby="dd9">
                            <li><a class="dropdown-item" href="#">Option 1</a></li>
                            <li><a class="dropdown-item" href="#">Option 2</a></li>
                        </ul>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody><?php
            $delCount = 0;
            while($query->have_posts()){ $query->the_post();
                $deleted = get_post_meta($post->ID, 'deleted', true);
                $cssClass = ( $deleted ) ? ' tr-data-deleted' : '';
                if($deleted) $delCount++;
            ?><tr data-id="<?php the_id(); ?>" class="client-tr <?php echo $cssClass; ?>">
            <td class="col-pt-clients">
                <div class="td-boxs d-inline-flex align-items-center w-100">
                    <div class="td-checkbox-box">
                        <input name="show-download-strip[]" class="form-check-input td-checkbox show-download-strip" type="checkbox" id="chk<?php the_id(); ?>" value="<?php the_id(); ?>">
                    </div>
                    <div class="td-client-box d-inline-flex align-items-center">
                        <?php if(has_post_thumbnail()){
                            ?><div class="td-icon-box tib-media d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'd-block mx-auto w-100' ));
                            ?></div><?php
                        }
                        else {
                            ?><div class="td-icon-box tib-letter d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo $ts->get_client_title($post->ID);
                            ?></div><?php
                        } ?>
                        <div class="td-title fw-light"><strong class="fw-medium"><?php the_title(); ?> </strong></div>
                    </div>
                </div>
            </td>
            <td class="col-pt-default">
                
            </td>
            <td class="col-pt-default">
                
            </td>
            <td class="col-pt-default">
                
            </td>
            <td class="col-pt-default">
                <a href="<?php echo get_permalink(); ?>" class="btn btn-view d-inline-flex align-items-center justify-content-center rounded-3 p-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                </a>
            </td>
        </tr><?php
        }
    ?></table>
</div>
<?php
    $ts->pt_v3_numeric_posts_nav($query); 
    ?><script><?php
        if($delCount > 0){?>
            document.getElementById("hide-deleted").innerHTML = "Hide Deleted (<?php echo $delCount; ?>)";
        <?php }
        else {?>
            document.getElementById("hide-deleted").style.display = "none";
        <?php }?>
        </script>
        <div class="floating-button-container position-fixed d-flex align-items-center flex-column">
            <div class="floating-button-panel">
                <button class="btn btn-float-add rounded-pill p-0 mb-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                </button>
                <div class="floating-button-group d-inline-flex flex-column position-absolute end-0 rounded-2">
                    <button type="button" class="btn rounded-2" data-bs-toggle="modal" data-bs-target="#modalMailStart">Send Email</button>
                    <button type="button" class="btn rounded-2" data-bs-toggle="modal" data-bs-target="#modalDownloads">Download as</button>
                </div>
            </div>
            <button class="btn btn-float-main rounded-pill p-0 d-inline-block align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
            </button>
        </div>
    <?php
}
else echo __('Nothing found!');
wp_reset_postdata();
?>