<?php 
$ts = new Theme_Customisations();
$args = array('post_type' => 'client', 'showposts' => -1);
$query = new WP_Query($args);
if($query->have_posts()){
    ?><ul class="data-scroll-list list-unstyled m-0 h-100" id="client-list"><?php
    while($query->have_posts()){ $query->the_post();
    ?><li>
            <a href="d-feed-feed.html" class="d-flex align-items-center border-end">
                <?php if(has_post_thumbnail()){
                    ?><div class="dsl-icon dsl-media d-flex align-items-center justify-content-center"><?php
                    echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'd-block mx-auto w-100' ));
                    ?></div><?php
                }
                else {
                    ?><div class="dsl-icon dsl-letters d-flex align-items-center justify-content-center"><?php
                    echo $ts->get_client_title($post->ID);
                    ?></div><?php
                } ?>
                <div class="dsl-content flex-grow-1 overflow-hidden">
                <div class="dsl-title text-nowrap text-truncate overflow-hidden"><?php the_title(); ?></div>
                <div class="dsl-text text-nowrap text-truncate overflow-hidden"><?php 
                $updated = get_the_modified_date('d-m-Y H:i:s');
                $now = date('d-m-Y H:i:s'); 
                
                $date1 = date_create($now);
                $date2 = date_create($updated);
                $diff = date_diff($date1,$date2);

                if( $diff->y > 0 ) echo __('More than a year ago');
                elseif( $diff->m > 0 ) echo __('More than a month ago');
                elseif($diff->d > 0 ) echo $diff->d . __(' day(s) ago');
                elseif($diff->h > 0 ) echo $diff->h . __(' hour(s) ago');
                else echo $diff->i . __(' minute(s) ago');
                ?></div>
                </div>
            </a>
            <details class="d-none"><?php the_title(); ?></details>
        </li><?php
    }
    ?></ul><?php
}
else echo '<p>'.__('Nothing found!').'</p>'; ?>