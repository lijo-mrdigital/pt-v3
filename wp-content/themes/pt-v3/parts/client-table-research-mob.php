<?php 
$ts = new Theme_Customisations();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$per_page = 20;
$args = array('post_type' => 'client', 'posts_per_page' => $per_page, 'paged' => $paged);
$query = new WP_Query($args);
$total_pages = ceil( $query->found_posts / $per_page );
if($query->have_posts()){
    ?><?php
            while($query->have_posts()){ $query->the_post();
                $deleted = get_post_meta($post->ID, 'deleted', true);
                $cssClass = ( $deleted ) ? ' task-list-item-deleted' : '';
            ?><li class="task-list-item <?php echo $cssClass; ?>">
                <div class="task-panel position-relative d-flex align-items-start">
                    <div class="task-checkbox-box position-absolute start-0">
                        <input class="form-check-input task-checkbox" type="checkbox" id="chk<?php echo $post->ID; ?>" value="<?php echo $post->ID; ?>">
                    </div>
                    <a href="#" class="task-panel-icon task-panel-icon-media d-flex align-items-center justify-content-center p-1 rounded-3">
                        <?php if(has_post_thumbnail()){
                            ?><div class="td-icon-box tib-media d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'd-block mx-auto w-100' ));
                            ?></div><?php
                        }
                        else {
                            ?><div class="td-icon-box tib-letter d-inline-flex align-items-center justify-content-center rounded-3 p-1"><?php
                            echo $ts->get_client_title($post->ID);
                            ?></div><?php
                        } ?>
                    </a>
                    <div class="ps-3 w-100 position-relative">
                        <h2 class="task-title fz-14 fw-light mb-2"><strong class="d-inline fw-medium">Hearing Direct</strong> – UK</h2>
                        <div class="task-box d-inline-flex align-items-center">
                        <div class="task-box-label">Briefs</div>
                        <span class="task-box-sep px-2">:</span>
                        <div class="task-box-txt fz-12">Yes</div>
                        </div>
                        <div class="task-box d-inline-flex align-items-center">
                        <div class="task-box-label">Messaging</div>
                        <span class="task-box-sep px-2">:</span>
                        <div class="task-box-txt fz-12">2</div>
                        </div>
                        <div class="task-box d-inline-flex align-items-center">
                        <div class="task-box-label">Audience Profile</div>
                        <span class="task-box-sep px-2">:</span>
                        <div class="task-box-txt fz-12">2</div>
                        </div>
                        <a href="#" class="position-absolute top-0 start-0 w-100 h-100"></a>
                    </div>
                </div>
            </li><?php
        }
    ?>
<?php $ts->pt_v3_numeric_posts_nav($query); 
}
else echo __('Nothing found!');
wp_reset_postdata();
?>