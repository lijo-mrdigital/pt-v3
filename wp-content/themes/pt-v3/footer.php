                    </div>
                </div>
            </div>
            
            <?php if(is_page_template( 'template-onboarding.php' ) || is_page_template( 'template-research.php' )) {?>
            <div class="modal fade modal-buttons modal-mail-start" id="modalMailStart" tabindex="-1" aria-labelledby="modalMailStartLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content border-0">
                        <div class="modal-header border-bottom-0">
                            <h5 class="modal-title fw-medium">Send Email</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <button class="btn btn-modal-xxl fw-semi-bold d-flex align-items-center justify-content-center rounded-3 d-block w-100 next-step" data-bs-target="#modalMailStepTwo" data-bs-toggle="modal">Choose from Template</button>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-modal-xxl fw-semi-bold d-flex align-items-center justify-content-center rounded-3 d-block w-100 next-step" data-bs-target="#modalMailStepThree" data-bs-toggle="modal">Custom Email</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade modal-default modal-mail-step-two" id="modalMailStepTwo" tabindex="-1" aria-labelledby="modalMailStepTwoLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content border-0 shadow">
                        <div class="modal-header border-bottom-0">
                            <h5 class="modal-title fw-medium">Send Email</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="button-check-group">
                                <div class="d-block fz-14 fw-medium mb-2">Select Template</div>
                                <div class="position-relative my-2 my-md-3">
                                    <input type="radio" class="btn-check" name="options" id="option1" checked>
                                    <label class="btn btn-block w-100 text-start btn-check-label" for="option1">Client Or Project Brief</label>
                                </div>
                                <div class="position-relative my-2 my-md-3">
                                    <input type="radio" class="btn-check" name="options" id="option2" autocomplete="off">
                                    <label class="btn btn-block w-100 text-start btn-check-label" for="option2">Performance Tracker</label>
                                </div>
                                <div class="position-relative my-2 my-md-3">
                                    <input type="radio" class="btn-check" name="options" id="option3" autocomplete="off">
                                    <label class="btn btn-block w-100 text-start btn-check-label" for="option3">Productive And Budge</label>
                                </div>
                                <div class="position-relative my-2 my-md-3">
                                    <input type="radio" class="btn-check" name="options" id="option4" autocomplete="off">
                                    <label class="btn btn-block w-100 text-start btn-check-label" for="option4">Payment Setup</label>
                                </div>
                                <div class="position-relative my-2 my-md-3">
                                    <input type="radio" class="btn-check" name="options" id="option5" autocomplete="off">
                                    <label class="btn btn-block w-100 text-start btn-check-label" for="option5">Request Access</label>
                                </div>
                            <div class="position-relative my-2 my-md-3 mb-0">
                                <input type="radio" class="btn-check" name="options" id="option6" autocomplete="off">
                                <label class="btn btn-block w-100 text-start btn-check-label" for="option6">Google Data Studio</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer position-relative border-top-0 justify-content-between">
                        <button type="button" class="btn btn-modal btn-light prev-step" data-bs-target="#modalMailStart" data-bs-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>
                            Back
                        </button>
                        <button type="button" class="btn btn-modal btn-danger next-step" data-bs-target="#modalMailStepThree" data-bs-toggle="modal">Next</button>
                    </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-default modal-mail-step-three" id="modalMailStepThree" tabindex="-1" aria-labelledby="modalMailStepThreeLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content border-0 shadow">
                        <div class="modal-header border-bottom-0">
                            <h5 class="modal-title fw-medium">Send Email</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="fz-14 fw-medium mb-3">Selected Template : Client or Project Brief</div>
                            <?php echo do_shortcode('[wpforms id="41" title="false"]'); ?>
                        </div>
                        <div class="modal-footer position-relative border-top-0 justify-content-between">
                            <button type="button" class="btn btn-modal btn-light prev-step" data-bs-target="#modalMailStepTwo" data-bs-toggle="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>
                                Back
                            </button>
                            <button type="button" class="btn btn-modal btn-danger" id="send-email">Send Email</button>
                    </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            <div class="modal fade modal-default modal-new-onboarding" id="modalNewOnboarding" tabindex="-1" aria-labelledby="modalNewOnboardingLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content border-0">
                        <div class="modal-header border-bottom-0">
                            <h5 class="modal-title fw-medium">Add New Project</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <?php echo do_shortcode('[wpforms id="39" title="false"]'); ?>
                            <!-- <form id="add-project" method="post">
                                <div class="form-group">
                                    <label class="form-label fz-14 fw-medium">Project Name</label>
                                    <input name="project-name" type="text" class="form-control required" placeholder="Project Name" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label fz-14 fw-medium">Project Logo</label>
                                    <input name="project_logo" class="form-control required" type="file" id="formFile" required accept="image/*">
                                    <div id="emailHelp" class="form-text">Size 1:1, file formats .png or .jpg</div>
                                </div>
                                <div class="form-group form-group-button">
                                    <button class="btn btn-danger d-block w-100">Submit</button>
                                </div>
                            </form> -->
                        </div>
                    </div>
                </div>
            </div>
            
            <?php wp_footer(); ?>
        </body>
        </html>