<?php
define('T_ROOT', get_template_directory_uri());
if ( ! function_exists( 'pt_v3_styles' ) ) :

	/**
	 * Enqueue styles.
	 *
	 * @return void
	 */
	function pt_v3_styles() {
		// Register theme stylesheet.
		$theme_version = wp_get_theme()->get( 'Version' );

		$version_string = is_string( $theme_version ) ? $theme_version : false;
		wp_register_style(
			'pt_v3-bootstrap',
			T_ROOT . '/assets/css/bootstrap.min.css',
			array(),
			$version_string
		);
		wp_register_style(
			'pt_v3-datepicker',
			T_ROOT . '/assets/css/bootstrap-datepicker.min.css',
			array(),
			$version_string
		);
		wp_register_style(
			'pt_v3-owl',
			T_ROOT . '/assets/css/owl.carousel.css',
			array(),
			$version_string
		);
		wp_register_style(
			'pt_v3-editor',
			T_ROOT . '/assets/css/trumbowyg.min.css',
			array(),
			$version_string
		);
		wp_register_style(
			'pt_v3-style',
			T_ROOT . '/assets/css/style.css',
			array(),
			$version_string
		);
		if(wp_is_mobile()){
			wp_register_style(
				'pt_v3-style-mobile',
				T_ROOT . '/assets/css/style-mobile.css',
				array(),
				$version_string
			);
		}

		// Enqueue theme stylesheet.
		wp_enqueue_style( 'pt_v3-bootstrap' );
		wp_enqueue_style( 'pt_v3-datepicker' );
		wp_enqueue_style( 'pt_v3-owl' );
		wp_enqueue_style( 'pt_v3-editor' );
		wp_enqueue_style( 'pt_v3-style' );
		if(wp_is_mobile()) wp_enqueue_style( 'pt_v3-style-mobile' );

		wp_register_script(
			'pt_v3-jquery',
			T_ROOT . '/assets/js/jquery-3.6.0.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-bootstrap',
			T_ROOT . '/assets/js/bootstrap.bundle.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-datepicker',
			T_ROOT . '/assets/js/bootstrap-datepicker.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-owl',
			T_ROOT . '/assets/js/owl.carousel.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-validator',
			T_ROOT . '/assets/js/jquery.validate.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-editor',
			T_ROOT . '/assets/js/trumbowyg.min.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-script',
			T_ROOT . '/assets/js/script.js',
			array(),
			$version_string,
			true
		);
		wp_register_script(
			'pt_v3-ajax',
			T_ROOT . '/assets/js/ajax.js',
			array(),
			$version_string,
			true
		);

		// Enqueue theme scripts.
		wp_enqueue_script( 'pt_v3-jquery' );
		wp_enqueue_script( 'pt_v3-bootstrap' );
		wp_enqueue_script( 'pt_v3-datepicker' );
		wp_enqueue_script( 'pt_v3-owl' );
		wp_enqueue_script( 'pt_v3-validator' );
		wp_enqueue_script( 'pt_v3-editor' );
		wp_enqueue_script( 'pt_v3-ajax' );
		wp_enqueue_script( 'pt_v3-script' );

		// Localize script
		wp_localize_script( 'pt_v3-ajax', 'frontend_ajax_object',
			array( 
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'security' => wp_create_nonce( 'nonce' ),
				'templateUrl' => get_stylesheet_directory_uri()
			)
		);

	}

endif;

add_action( 'wp_enqueue_scripts', 'pt_v3_styles' );

if ( ! function_exists( 'pt_v3_preload_webfonts' ) ) :

	/**
	 * Preloads the main web font to improve performance.
	 *
	 * Only the main web font (font-style: normal) is preloaded here since that font is always relevant (it is used
	 * on every heading, for example). The other font is only needed if there is any applicable content in italic style,
	 * and therefore preloading it would in most cases regress performance when that font would otherwise not be loaded
	 * at all.
	 *
	 * @return void
	 */
	function pt_v3_preload_webfonts() {
		?>
		<link rel="preload" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" as="font" type="font/woff2" crossorigin>
		<?php
	}

endif;

add_action( 'wp_head', 'pt_v3_preload_webfonts' );

require get_template_directory() . '/inc/theme-setup.php';

?>