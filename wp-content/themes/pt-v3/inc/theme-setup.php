<?php
/**
 * The 'main' function
 *
 * @return void
 */
function theme_customisations_main() {
    new Theme_Customisations();
}

class Theme_Customisations {
    /**
	 * Set up the app
	 */
    public function __construct() {
        add_action( 'init', array( $this, 'pt_v3_register_pt' ), 10 );
        add_action( 'init', array( $this, 'pt_v3_register_nav_menu' ), 10 );
        add_action( 'init', array( $this, 'pt_v3_create_table' ), 10 );
        add_filter( 'wp_nav_menu', array( $this, 'pt_v3_add_menuclass') );
        add_action( 'wp_ajax_update_client', array( $this, 'pt_v3_update_client' ) );
        add_action( 'wp_ajax_delete_client_meta', array( $this, 'pt_v3_delete_client_meta' ) );
        add_action( 'wp_ajax_delete_client', array( $this, 'pt_v3_delete_client' ) );
        add_action( 'wp_ajax_download_client', array( $this, 'pt_v3_download_client' ) );
        add_action( 'wp_ajax_add_project', array( $this, 'pt_v3_add_project' ) );
        add_action( 'template_redirect', array( $this, 'pt_v3_recent_client' ) );
        add_action( 'wpforms_process_complete', array( $this, 'pt_v3_save_project' ), 10, 4 );
        add_action( 'pre_get_posts', array( $this, 'pt_v3_pre_get_posts_hidden' ), 9999 );
        add_filter( 'show_admin_bar', '__return_false' );
        add_theme_support( 'post-thumbnails' );
	}

    /**
	 * Register post types
	 */
    public function pt_v3_register_pt() {
        /* $labels = array(
            'name'                  => _x( 'Projects', 'Post type general name', 'project' ),
            'singular_name'         => _x( 'Project', 'Post type singular name', 'project' ),
            'menu_name'             => _x( 'Projects', 'Admin Menu text', 'project' ),
            'name_admin_bar'        => _x( 'Project', 'Add New on Toolbar', 'project' ),
            'add_new'               => __( 'Add New', 'project' ),
            'add_new_item'          => __( 'Add New Project', 'project' ),
            'new_item'              => __( 'New Project', 'project' ),
            'edit_item'             => __( 'Edit Project', 'project' ),
            'view_item'             => __( 'View Project', 'project' ),
            'all_items'             => __( 'All Projects', 'project' ),
            'search_items'          => __( 'Search Projects', 'project' ),
            'parent_item_colon'     => __( 'Parent Projects:', 'project' ),
            'not_found'             => __( 'No Projects found.', 'project' ),
            'not_found_in_trash'    => __( 'No Projects found in Trash.', 'project' ),
        );     
        $args = array(
            'labels'             => $labels,
            'description'        => 'Project custom post type.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'project' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 20,
            'menu_icon'          => 'dashicons-media-document',
            'supports'           => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies'         => array( 'category', 'post_tag' ),
            'show_in_rest'       => true
        );
        
        register_post_type( 'Project', $args ); */
        
        $labels = array(
            'name'                  => _x( 'Clients', 'Post type general name', 'pt_v3' ),
            'singular_name'         => _x( 'Client', 'Post type singular name', 'pt_v3' ),
            'menu_name'             => _x( 'Clients', 'Admin Menu text', 'pt_v3' ),
            'name_admin_bar'        => _x( 'client', 'Add New on Toolbar', 'pt_v3' ),
            'add_new'               => __( 'Add New', 'pt_v3' ),
            'add_new_item'          => __( 'Add New client', 'pt_v3' ),
            'new_item'              => __( 'New client', 'pt_v3' ),
            'edit_item'             => __( 'Edit client', 'pt_v3' ),
            'view_item'             => __( 'View client', 'pt_v3' ),
            'all_items'             => __( 'All Clients', 'pt_v3' ),
            'search_items'          => __( 'Search Clients', 'pt_v3' ),
            'parent_item_colon'     => __( 'Parent Clients:', 'pt_v3' ),
            'not_found'             => __( 'No Clients found.', 'pt_v3' ),
            'not_found_in_trash'    => __( 'No Clients found in Trash.', 'pt_v3' ),
        );     
        $args = array(
            'labels'             => $labels,
            'description'        => 'Client custom post type.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'client' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 20,
            'menu_icon'          => 'dashicons-format-chat',
            'supports'           => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies'         => array( 'category', 'post_tag' ),
            'show_in_rest'       => true
        );
        
        register_post_type( 'Client', $args );
    }

    /**
	 * Register menus
	 */
    public function pt_v3_register_nav_menu(){
        register_nav_menus( array(
            'primary_menu' => __( 'Primary Menu', 'pt_v3' ),
            'footer_menu'  => __( 'Footer Menu', 'pt_v3' ),
        ) );
    }

    /**
	 * Add class on <a>
	 */
    function pt_v3_add_menuclass($ulclass) {
        return preg_replace('/<a /', '<a class="menu-link"', $ulclass);
    }

    /**
     * Pagination
     */
    public function pt_v3_numeric_posts_nav($query) {
        
        /** Stop execution if there's only 1 page */
        if( $query->max_num_pages <= 1 ) return;
        
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $query->max_num_pages );
        
        /** Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;
        
        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }
        
        if ( ( $paged + 2 ) <= $max ) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }
        
        echo '<div class="pagination-panel"><ul class="pagination pagination-alt rounded-0 mb-0">' . "\n";
        
        /** Previous Post Link */
        /* if ( get_previous_posts_link() )
            printf( '<li>%s</li>' . "\n", get_previous_posts_link() ); */
        
        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active page-item"' : ' class="page-item"';
        
            printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
        
            if ( ! in_array( 2, $links ) )
                echo '<li class="page-item">…</li>';
        }
        
        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active page-item"' : ' class="page-item"';
            printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }
        
        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) )
                echo '<li>…</li>' . "\n";
        
            $class = $paged == $max ? ' class="active page-item"' : ' class="page-item"';
            printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }
        
        /** Next Post Link */
        /* if ( get_next_posts_link() )
            printf( '<li class="page-item">%s</li>' . "\n", get_next_posts_link() ); */
        
        echo '</ul></div>' . "\n";
        
    }

    /* 
     * Update client 
     */
    public function pt_v3_update_client(){
        if(empty($_POST)) return;
        $status_id = update_post_meta($_POST['post_id'], $_POST['meta'], $_POST['status']);
        if($status_id) {
            $return = array( 'updated' => $status_id, 'status' => $_POST['status']);
            echo json_encode($return);
        }
        else echo $status_id;
        wp_die();
    }
    
    /* 
     * Delete client meta
     */
    public function pt_v3_delete_client_meta(){
        if(empty($_POST)) return;
        $deleted = delete_post_meta($_POST['post_id'], $_POST['meta']);
        if($deleted) {
            $return = array( 'updated' => $deleted);
            echo json_encode($return);
        }
        else echo $deleted;
        wp_die();
    }
    
    /* 
     * Delete client 
     */
    public function pt_v3_delete_client(){
        if(empty($_POST)) return;
        foreach($_POST['post_id'] as $post_id){
            if( update_post_meta($post_id, 'deleted', 1) ){
                $return[] = $post_id;
            }
        }
        echo json_encode($return);
        wp_die();
    }

    /* 
     * Restrict pages for guets
     */
    public function pt_v3_pre_get_posts_hidden(){
        if( ! is_user_logged_in() ) {
            wp_safe_redirect( admin_url() );
            exit;
        }
    }

    public function pt_v3_add_project(){
        //if(empty($_POST)) return;
        check_ajax_referer('file_upload', 'security');
        $arr_img_ext = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
        print_r($_FILES['project_logo']['type']);
        if (in_array($_FILES['project_logo']['type'], $arr_img_ext)) {
            echo 'test';
            print_r(file_get_contents($_FILES["project_logo"]["tmp_name"]));
            //$upload = wp_upload_bits($_FILES["project_logo"]["name"], null, file_get_contents($_FILES["project_logo"]["tmp_name"]));
        }
        /* $postarr = array(
            'post_title' => $_POST['projectName'],
            'post_type' => 'client'
        );
        $post_id = wp_insert_post( $postarr );
        echo $post_id; */
        
        // Add Featured Image to Post
        /* $image_url        = $_POST['logo']; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $post_id, $attach_id ); */
        wp_die();
    }

    /* 
     * Get client title
     */
    public function get_client_title($post_id){
        if(strpos(get_the_title($post_id), ' ')){
            $title = explode(' ', get_the_title($post_id));
            return substr($title[0], 0, 1) . substr($title[1], 0, 1);
        }
        else return substr(get_the_title($post_id), 0, 1);
    }

    public function pt_v3_download_client(){
        if(empty($_POST)) return;
        // Get WordPress uploads directory.
        $upload_dir = wp_upload_dir();
        $downloadfile = $upload_dir['baseurl'];
        $upload_dir = $upload_dir['basedir'];
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="sample.csv"');
        $filename = '/clients_'.date('Ymdhsi').'.csv';
        $filepath = $upload_dir . $filename;
        $downloadfile = $downloadfile . $filename;
        $file     = fopen( $filepath, 'w' );
        $metas['header'] = array('Client', 'Briefing', 'Performance Tracker', 'Productive & Budget');
        foreach($_POST['clients'] as $client){
            $metas[$client][] = get_the_title($client);
            $metas[$client][] = get_post_meta($client, 'briefing', true);
            $metas[$client][] = get_post_meta($client, 'pt_setup', true);
            $metas[$client][] = get_post_meta($client, 'prod_bud_setup', true);
        }
        foreach($metas as $meta){
            fputcsv($file, $meta);
        }
        fclose($file);
        echo $downloadfile;
        wp_die();
    }

    public function pt_v3_create_table(){
        global $wpdb;
        $db_version = '1.0';

        $table_name = $wpdb->prefix . 'recent_clients';
        
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            client_id mediumint(9) NOT NULL,
            user_id mediumint(9) NOT NULL,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        add_option( 'db_version', $db_version );
    }

    public function pt_v3_recent_client(){
        if(! is_singular('client')) return;
        global $wpdb;
        $table_name = $wpdb->prefix . 'recent_clients';
        $wpdb->insert( 
            $table_name, 
            array( 
                'client_id' => get_the_id(), 
                'time' => current_time( 'mysql' ), 
                'user_id' => get_current_user_id(), 
            )
        );
    }

    public function pt_v3_save_project( $fields, $entry, $form_data, $entry_id ) {
        if ( absint( $form_data['id'] ) !== 39 ) return;
        $entry = wpforms()->entry->get( $entry_id );
        $entry_fields = json_decode( $entry->fields, true );
        $title = $entry_fields[1]['value'];
        $logo  = $entry_fields[2]['value'];
        $postarr = array(
            'post_title' => $title,
            'post_type' => $logo
        );
        $post_id = wp_insert_post( $postarr );
        echo $post_id;
    }

}

add_action('init', 'theme_customisations_main', -1 );
?>